const btn = document.querySelector('button')

btn.addEventListener('click', login)

function login() {
  const emailInput = document.getElementById('email').value

  const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    
  if(emailRegex.test(emailInput)) {
  document.getElementById('login').innerHTML = 'Seu e-mail foi cadastrado com sucesso!'
} 
}

